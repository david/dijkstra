#ifndef __HEAP_SORT_HPP__
#define __HEAP_SORT_HPP__
#include"min_binary_heap.hpp"

/**
   This function reallocates the Binary Heap only when necessary,
   that is when the size the user asks for is greater than the current one.
   @ensures size > 0
   @param size is the maximal size of the binary heap asked by the user.
   @return an allocated binary heap.
 */
MinBinaryHeap<int> * initialize_heap(size_t size);

/**
   Sorts the permutation the a binary heap
   Time complexity: O(n log n)
   Space complexity: O(n)
   @param permutation is the permutation we want to sort
   @param size is the size of the permutation
   @return the number of swaps made during the sort.
*/
size_t heap_sort(int * permutation, size_t size);

#endif
