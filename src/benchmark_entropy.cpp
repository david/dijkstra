#include<iostream>
#include<time.h>
#include<math.h>
#include"heap_sort.hpp"
#include"random_generators.hpp"
#include"entropy.hpp"


int main(int argc, char ** argv){
  size_t max_size = 500;
  int permutation[max_size];
  long double distribution[max_size];
  long double entropy;
  int size, xp;
  int xp_num = 1000;
  long double mean;
  long double mean_time;
  clock_t temps_deb, temps_fin;
  srand(time(NULL));
  
  for(entropy = 0.1; entropy <= log2(max_size); entropy+= 0.1){
    mean = 0;
    mean_time = 0;
    for(xp = 0; xp < xp_num; xp++){
      if(xp%10 == 0)
	random_distribution_generator(distribution, entropy, max_size, 100000);
      random_tabular_with_fixed_distribution(permutation, distribution, max_size);
      temps_deb = clock();
      mean +=heap_sort(permutation, max_size);
      temps_fin = clock();
      mean_time += (long double)(temps_fin - temps_deb)/(long double)CLOCKS_PER_SEC;
    }
    printf("%LF %LF %LF\n", entropy, mean/xp_num, mean_time/xp_num);
  }
  return EXIT_SUCCESS;
}
