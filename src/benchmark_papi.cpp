#include<iostream>
#include<papi.h>
#include"heap_sort.hpp"
#include"random_generators.hpp"


#define PAPI_events_number 4
#define ERROR_RETURN(retval) { fprintf(stderr, "Error %d, %s, %s:line %d: \n", retval, PAPI_strerror(\
retval), __FILE__,__LINE__);  exit(retval); }

int set_PAPI(){
  int eventSet = PAPI_NULL;
  int retval;
  int events[PAPI_events_number] = {PAPI_TOT_INS, PAPI_BR_MSP, PAPI_L1_DCM, PAPI_L2_DCM};  

  int i;

  /* We use number to keep track of the number of events in the eventSet */
  if((retval = PAPI_library_init(PAPI_VER_CURRENT)) != PAPI_VER_CURRENT )
    ERROR_RETURN(retval);

  /* Creating the eventset */
  if ( (retval = PAPI_create_eventset(&eventSet)) != PAPI_OK)
    ERROR_RETURN(retval);

  /* Fill eventSet */
  for (i = 0; i < PAPI_events_number; ++i){
    if ( (retval = PAPI_add_event(eventSet, events[i])) != PAPI_OK)
      ERROR_RETURN(retval);
  }
  return eventSet;
}


int main(int argc, char ** argv){
  size_t max_size = 4096;
  int permutation[max_size];
  int size, xp;
  int xp_num = 10000;
  long long values[PAPI_events_number];
  int retval;
  long long int nb_instructions, nb_branch_fault;
  long long int l1_misses, l2_misses, l3_misses;
  int eventSet = set_PAPI(); 
  
  for(size = 4; size <= max_size; size = 2*size){
    nb_instructions = 0;
    nb_branch_fault = 0;
    l1_misses = l2_misses = l3_misses = 0;
    for(xp = 0; xp < xp_num; xp++){
      random_permutation(permutation, size);
      if ( (retval = PAPI_start(eventSet)) != PAPI_OK)
	ERROR_RETURN(retval);
      heap_sort(permutation, size);
      if ( (retval = PAPI_stop(eventSet, values)) != PAPI_OK)
	ERROR_RETURN(retval);
      nb_instructions += values[0];
      nb_branch_fault += values[1];
      l1_misses += values[2];
      l2_misses += values[3];
      //l3_misses += values[4];
    }
    printf("%d %lf %lf %lf %lf\n", size, nb_instructions/(double)xp_num, nb_branch_fault/(double)xp_num, l1_misses/(double)xp_num, l2_misses/(double)xp_num);
  }
  return EXIT_SUCCESS;
}

