#ifndef __MAX_BINARY_HEAP_HPP__
#define __MAX_BINARY_HEAP_HPP__
#include<iostream>

#include"binary_heap.hpp"

template <typename T>
class MaxBinaryHeap: public BinaryHeap<T>{
protected:
  bool heap_cmp(T & value1, T & value2){
    return value1 > value2;
  }

public:
  MaxBinaryHeap(size_t max): BinaryHeap<T>(max){}
  
};


#endif
