#ifndef __MIN_BINARY_HEAP_HPP__
#define __MIN_BINARY_HEAP_HPP__
#include<iostream>

#include"binary_heap.hpp"

template <typename T>
class MinBinaryHeap: public BinaryHeap<T>{
protected:
  bool heap_cmp(T & value1, T & value2){
    return value1 < value2;
  }

public:
  MinBinaryHeap(size_t max): BinaryHeap<T>(max){}
  
};


#endif
