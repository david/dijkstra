#ifndef __BINARY_HEAP_HPP__
#define __BINARY_HEAP_HPP__
#include<iostream>
#include <exception>

class FullBinaryHeapException: public std::exception{
  virtual const char* what() const throw()
  {
    return "Cannot add value in a full Heap";
  }
};



template <typename T>
class BinaryHeap{
private:
  T * data;
  size_t size;
  size_t max;
  size_t counter_swap_up;
  size_t counter_swap_down;

protected:
  virtual bool heap_cmp(T & value1, T & value2) = 0;
  
public:
  /**
     Class constructor.
     Creates a heap of at most 'max' elements and initializes swap counters to zero.
     @input max is the maximal number of elements the heap can contain
  */
  BinaryHeap(size_t max){
    this->max = max;
    this->size = 0;
    this->data = new T[max];
    counter_swap_up = 0;
    counter_swap_down = 0;
  }

  ~BinaryHeap(){
    delete [] this->data;
  }

  bool isEmpty(){
    return size == 0;
  }

  size_t getSize(){
    return this->size;
  }

  /**
     This method is used during benchmarks, when one wants to know
     how many time the swap function has been called.
     It is to be called at the beginning of each benchmark.
  */
  void resetCounters(){
    counter_swap_up = 0;
    counter_swap_down = 0;
  }

  size_t getSwapsCounters(){
    return counter_swap_down + counter_swap_up;
  }
  
  /**
     Returns the position of a node's parent
     @requires 0 < position < this->size
     @param position is the position of a node of the heap, except the root
     @return the position of a node's parent
   */
  inline int parent(size_t position){
    return (position-1)/2;
  }

  /**
     Returns the position of a node's left child
     @param position is the position of a node of the heap, except the root
     @return the position of a node's parent
  */
  inline int left_child(size_t position){
    return position*2+1;
  }

  inline int right_child(size_t position){
    return position*2+2;
  }
  
  /**
     This function takes the position of a value as an input
     and makes it go UP in the heap until the comparison constraint is satisfied.
     @param position is the position of the value we want to check
   */
  void heapify_up(size_t position){
    while(position > 0 && heap_cmp(data[position], data[parent(position)])){
      std::swap(data[parent(position)], data[position]);
      counter_swap_up++;
      position = parent(position);
    }
  }

  /**
     This function takes the position of a value as an input
     and makes it go DOWN in the heap until the comparison constraint is satisfied.
     @param position is the position of the value we want to check
   */
  void heapify_down(size_t position){
    int min_child;
    min_child = heap_cmp(data[left_child(position)], data[right_child(position)])? left_child(position): right_child(position);
    while(min_child < (size-1) && heap_cmp(data[min_child], data[position])){
      std::swap(data[min_child], data[position]);
      position = min_child;
      if( right_child(position) < size-1 )
	min_child = heap_cmp(data[left_child(position)],data[right_child(position)])? left_child(position): right_child(position);
      else
	min_child = size - 1;
      counter_swap_down++;
    }
  }

  /* This function insert a value in the heap
     @param value is the value we want to add.
     @requires this->size != this-> max (the heap is not full)
     @ensures the size is incremented
     @exception throws a FullBinaryHeapException if the heap is full.
   */
  void insert(T value) {
    if(size == max)
      throw new FullBinaryHeapException();
    data[size++] = value;
    heapify_up(size-1);
  }

  /*
    Extract the value at the top of the heap
    @ensures the size is decremented
    @returns the value at the top of the heap
  */
  T extract(){
    std::swap(data[0], data[size-1]);
    heapify_down(0);
    return data[--size];
  }

  /**
     Outputs the content of the heap a a regular tabular
   */
  friend std::ostream& operator<<(std::ostream& os, const BinaryHeap<int>& heap);
  
};


static std::ostream& operator<<(std::ostream& os, const BinaryHeap<int>& heap)
{
  for( int i = 0; i < heap.size; i++)
    os<<heap.data[i]<<" ";
  os<<std::endl;
  return os;
}


#endif
