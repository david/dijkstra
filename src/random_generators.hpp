#ifndef __RANDOM_GENERATORS_HPP__
#define __RANDOM_GENERATORS_HPP__
#include<iostream>


/**
   Creates a random permutation of size 'size'
   @param permutation is the tabular in which the permutation will be stored
   @param size is the size of the permutation
   @ensures size > 0
 */
void random_permutation(int * permutation, size_t size);


/**
   Fills a tabular with random i.i.d. values drawn according to a given distribution
   @param permutation is the tabular in which the result will be stored
   @param dist is the distribution used to draw random values
   @param size is the size of the permutation
   @ensures size > 0
   @ensures size(tab) = size(dist)
 */
void random_tabular_with_fixed_distribution(int * tab, long double * dist, size_t size);

#endif
