#include<iostream>
#include<time.h>
#include"heap_sort.hpp"
#include"random_generators.hpp"



int main(int argc, char ** argv){
  size_t max_size = 65536;
  int permutation[max_size];
  int size, xp;
  int xp_num = 1000;
  double mean;
  double mean_time;
  clock_t temps_deb, temps_fin;
  
  for(size = 4; size <= max_size; size = 2*size){
    mean = 0;
    mean_time = 0;
    for(xp = 0; xp < xp_num; xp++){
      random_permutation(permutation, size);
      temps_deb = clock();
      mean +=heap_sort(permutation, size);
      temps_fin = clock();
      mean_time += (long double)(temps_fin - temps_deb)/(long double)CLOCKS_PER_SEC;
    }
    printf("%d %lf %lf\n", size, mean/xp_num, mean_time/xp_num);
  }
  return EXIT_SUCCESS;
}
