CPPFLAGS= -O3 -g


all: benchmark benchmark_papi benchmark_entropy sort_entropy

benchmark: src/heap_sort.o src/random_generators.o src/benchmark.o src/entropy.o
	g++ $(CPPFLAGS) -o benchmark src/heap_sort.o src/random_generators.o src/benchmark.o src/entropy.o

benchmark_papi: src/heap_sort.o src/random_generators.o src/benchmark_papi.o src/entropy.o
	g++ $(CPPFLAGS) -o benchmark_papi src/heap_sort.o src/random_generators.o src/benchmark_papi.o src/entropy.o -lpapi

benchmark_entropy: src/heap_sort.o src/random_generators.o src/benchmark_entropy.o src/entropy.o
	g++ $(CPPFLAGS) -o benchmark_entropy src/heap_sort.o src/random_generators.o src/entropy.o src/benchmark_entropy.o -lm

sort_entropy: src/random_generators.o src/tris.o src/entropy.o
	g++ $(CPPFLAGS) -o sort_entropy src/random_generators.o src/tris.o src/entropy.o -lm


clean:
	rm -f src/*.o
	rm -f benchmark
	rm -f benchmark_papi
	rm -f benchmark_entropy
	rm -f sort_entropy
